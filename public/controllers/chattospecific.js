app.controller('ChatToSpecific', ["$stateParams","dataWarehouse","$scope","$state","CM",function($stateParams, dataWarehouse, $scope, $state, CM)
	{
		that=this;
		this.user = $stateParams.userName;						//CHATTING WITH WHICH USER
		dataWarehouse.chattingWith = $stateParams.userName;		//PUTTING CHATTING WITH IN SERVICE
		this.dw = dataWarehouse;								//FOR ACCESSING SERVICE DIRECT IN HTML
		this.message = "";										//KEEP TEXT BOX CLEAR


		// if(!parseInt(this.user))
		// CM.findIndexAndMergeMsg(this.user, "users", function(index) {
		// 	that.msgs=dataWarehouse.data.users[index].msgs;
		// });
		// else
		// 	CM.findIndexAndMergeMsg(this.user, "groups", function(index) {
		// 	that.msgs=dataWarehouse.data.users[index].msgs;
		// });

		

		for(var i=0; i<dataWarehouse.data.users.length; i++)
		{
			if(dataWarehouse.data.users[i].name == dataWarehouse.chattingWith)
			{
				dataWarehouse.data.users[i].msgs=dataWarehouse.data.users[i].msgs.concat(dataWarehouse.data.users[i].newMsgs);
				dataWarehouse.data.users[i].newMsgs=[];
				that.msgs = dataWarehouse.data.users[i].msgs;
				that.pos = i;
				break;
			}
		}

		if(!(that.pos > -1))
		{
			for(var i=0; i<dataWarehouse.data.groups.length; i++)
			{

				if(dataWarehouse.data.groups[i].name == dataWarehouse.chattingWith)
				{
					dataWarehouse.data.groups[i].msgs=dataWarehouse.data.groups[i].msgs.concat(dataWarehouse.data.groups[i].newMsgs);
					dataWarehouse.data.groups[i].newMsgs=[];
					that.msgs = dataWarehouse.data.groups[i].msgs;
					that.pos = i;
					break;
				}
			}
		}



		for(var i=0; i<dataWarehouse.users.length; i++)
		{
			if(dataWarehouse.users[i].userName == that.user)
			{
				dataWarehouse.users[i].newMsgsCount = 0;
			}
		}

		for(var i=0; i<dataWarehouse.groups.length; i++)
		{
			if(dataWarehouse.groups[i].groupID == that.user)
			{
				dataWarehouse.groups[i].newMsgsCount = 0;
			}
		}

		this.groupView=function(user)
		{
			//console.log(user);
			var gid = Math.ceil(Math.random()*10000);
			dataWarehouse.groups.push({groupID : gid, members : [user], newMsgsCount : 0});
			dataWarehouse.data.groups.push({name: gid,msgs:[{ msg:"group created "+gid,from: "userAdd",sender: "mine"},
				{msg: user+"is added", from: "userAdd", sender: "mine"}],newMsgs:[]});

			chat.emit('createGroup', {id : gid, user : user});
			
			dataWarehouse.currentBuildingGroup = gid;
			dataWarehouse.addingToGroup = true;
			$state.go('chatwith',{userName:gid});
		}
		this.chatView = function() {

			dataWarehouse.addingToGroup = false;
		}
		
		this.sendMsg = function() {
			var temp=parseInt(dataWarehouse.chattingWith);
			if(!temp)
			{

				chat.emit('sendMessage', {content: that.message, userName : that.user}, function(success) {
					if(success)
					{
						$scope.$apply(function() {
							for(var j=0; j<that.msgs.length; j++)
								console.log('happening' + that.msgs[j].from);
							that.msgs.push({msg: that.message, from : "MyMsg"});
							that.message="";
					});
					}
				});
			}
			else
			{
				chat.emit('sendGroupMessage', {content: that.message, id : that.user}, function(success) {
					if(success)
					{
						$scope.$apply(function() {
							// for(var j=0; j<that.msgs.length; j++)
							// 	console.log('happening' + that.msgs[j].from);
							that.msgs.push({msg: that.message, from : "MyMsg"});
							that.message="";
					});
					}
				});
			}
		}

}]);