app.controller('Chat',["dataWarehouse","$scope","$state","CM",function(dataWarehouse, $scope, $state, CM) {
	that=this;
	ms = io();
	chat = io('/chat');
	console.log('inside chat');
	this.data = dataWarehouse;
	dataWarehouse.chattingWith="";
	//dataWarehouse.chattingWith="";
	var findNewOnes =  function(oldArr, newArr, cb) {
		result = [];
		for(var i=0; i<newArr.length; i++)
		{
			//if(!(oldArr.indexOf(newArr[i]) > -1))	TO BE CHECK
			if(oldArr.indexOf(newArr[i]) > -1)
			{}
			else{
				result.push(newArr[i]);
			}
		}
		cb(result);
	}
	var increaseBadgeCountByOne = function(id) {
		if(!parseInt(id))
		{
			for(var i=0; i<dataWarehouse.users.length; i++) {
				if(dataWarehouse.users[i].userName == id)	{
					dataWarehouse.users[i].newMsgsCount++;
				}
			}
		}
		else
		{
			for(var i=0; i<dataWarehouse.groups.length; i++) {
				if(dataWarehouse.groups[i].groupID == id)	{
					dataWarehouse.groups[i].newMsgsCount++;
				}	
			}
		}
	}

	this.addToGroup =  function(user)
	{
		for(var i=0; i<dataWarehouse.groups.length; i++)
		{
			if(dataWarehouse.groups[i].groupID == dataWarehouse.currentBuildingGroup && !(dataWarehouse.groups[i].members.indexOf(user) > -1))
			{
				chat.emit('addUserToGroup', { id : dataWarehouse.currentBuildingGroup, user : user});

				dataWarehouse.groups[i].members.push(user);
				
				for(var i=0; i<dataWarehouse.data.groups.length; i++)
				{
					if(dataWarehouse.data.groups[i].name == dataWarehouse.currentBuildingGroup)
						dataWarehouse.data.groups[i].msgs.push({msg: user+"is added", from: "userAdd", sender: "mine"});
				}
				break;
			}
		}
		
	}

	chat.on('groupMembersUpdated', function(data)
		{	if(data.sender != dataWarehouse.userName)
			for(group in dataWarehouse.data.groups)
			{
				if(dataWarehouse.data.groups[group].name == data.id)
				{
					for( i in dataWarehouse.groups)
						if(dataWarehouse.groups[i].groupID == data.id) {
							CM.findNewOnes(dataWarehouse.groups[i].members, data.members, function(res) {
								dataWarehouse.data.groups[group].msgs.push({msg: res[0]+"is added by"+ data.sender, from:"system" , sender:data.sender});
								dataWarehouse.groups[i].members.push(res[0]);
							});
						}
				}
			}
		});

	// chat.on('groupCreated',function(group) {
	// 	dataWarehouse.groups.push({groupID : group.id, members : [group.user], newMsgsCount : 2});
	// 	dataWarehouse.data.groups.push({name: gid, msgs : [],
	// 		newMsgs:[{ msg:"group created by "+group.id,from: "system",sender: "system"},
	// 		{msg: "you were added by "+group.id, from: "system", sender: "system"}]});
	// });

	// chat.on('userAddedToGroup', function(group) {
	// 	for(var i=0; i<dataWarehouse.groups.length; i++)
	// 	{
	// 		if(dataWarehouse.groups[i].groupID == group.groupID && !(dataWarehouse.groups[i].members.indexOf(group.user) > -1))
	// 		{				
	// 			dataWarehouse.groups[i].members.push(group.user);
				
	// 			for(var i=0; i<dataWarehouse.data.groups.length; i++)
	// 			{
	// 				if(dataWarehouse.data.groups[i].name == group.groupID)
	// 					dataWarehouse.data.groups[i].msgs.push({msg: group.user+" is added by "+group.adder, from: "system", sender: "system"});
	// 			}
	// 			break;
	// 		}
	// 	}
	// });

	chat.on('groupRequest', function(data) {
		$scope.$apply(function(){

			console.log('got groupRequest');
			dataWarehouse.groups.push({groupID : data.id, members : [], newMsgsCount : 0});
			for(member in data.members)
				dataWarehouse.groups[dataWarehouse.groups.length - 1].members.push(data.members[member]);
			dataWarehouse.groups[dataWarehouse.groups.length - 1].members.push(dataWarehouse.userName)

			dataWarehouse.data.groups.push({name: data.id, msgs : [], newMsgs: []});
			chat.emit('groupRequestAccepted',data.id);

		});
		
	});

	chat.on('giveUserName', function(data) {
		console.log('giverUserName called by server');
		chat.emit('sendUserName', dataWarehouse.userName);
	});

	chat.on('error', function(msg)
		{
			$state.go('login');

		});
	// chat.on('connected', function(data) {
	// 	console.log(data.msg);
	// });

	chat.on('userUpdated', function(users) {
		//console.log(users);
		$scope.$apply(function(){
			
			var newUsers = [];
			var oldUsers = [];
			for(var i=0; i<dataWarehouse.users.length; i++)
			{
				oldUsers.push(dataWarehouse.users[i].userName);
			}
			findNewOnes(oldUsers, users, function(data) {
				newUsers = data;
				//dataWarehouse.users=users;
				for(var i=0; i<newUsers.length; i++)
					dataWarehouse.users.push({userName:newUsers[i], newMsgsCount : 0});
			});

			

			//console.log(dataWarehouse.users);
			for(var i=0; i<newUsers.length; i++) {
				dataWarehouse.data.users[dataWarehouse.data.users.length]={name : newUsers[i], msgs : [], newMsgs : []};
			}
		});
	});

	chat.on('gotGroupMessage', function(message) {
		if(message.sender != dataWarehouse.userName)
			$scope.$apply(function() {

				for(var i=0; i<dataWarehouse.data.groups.length; i++)
					{
						if(dataWarehouse.data.groups[i].name == message.id)
						{
							if(message.id == dataWarehouse.chattingWith) {
								dataWarehouse.data.groups[i].msgs.push({
									msg : message.content,
									from : 'OtherMsg',
									sender : message.sender
								});
								break;
							}
							else {
								dataWarehouse.data.groups[i].newMsgs.push({
									msg : message.content,
									from : 'OtherMsg',
									sender : message.sender
								});
								increaseBadgeCountByOne(message.id);
								break;

							}
						}
					}

			});
	});

	chat.on('newMessage', function(message) {
		//console.log(message.content);
		$scope.$apply(function() {
				for(var i=0; i<dataWarehouse.data.users.length; i++)
				{
					if(dataWarehouse.data.users[i].name == message.userName)
					{
						if(message.userName == dataWarehouse.chattingWith) {
							dataWarehouse.data.users[i].msgs.push({
								msg : message.content,
								from : 'OtherMsg'
							});
							break;
						}
						else {
							dataWarehouse.data.users[i].newMsgs.push({
								msg : message.content,
								from : 'OtherMsg'
							});
							increaseBadgeCountByOne(message.userName);
							break;

						}
					}
				}
		});
		
	});
}]);