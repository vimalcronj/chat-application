var app = angular.module('chatApp',['ui.router']);

app.config(["$stateProvider", "$locationProvider", function($stateProvider, $locationProvider) {
	console.log("under routing");
	$stateProvider
	.state("login", {
		url : "/",
		templateUrl : "/templates/login.html",
		controller : "Login",
		controllerAs : "loginAs"
	})

	.state("chat", {
		url : "/chat",
		templateUrl : "/templates/chat.html",
		controller : "Chat",
		controllerAs : "chat"
	})
	.state("chatwith", {
		parent : "chat",
		templateUrl : "/templates/chatto.html",
		url : "/:userName",
		controller : "ChatToSpecific",
		controllerAs : "chattosome"
	});
}]);
