app.service('dataWarehouse', [function() {
	this.users = [];
	this.groups=[];
	this.sockets = [];
	this.chattingWith="";
	this.data= {
				users : [
					{
					name: "dummy",
					msgs:[
						{
							msg:"hello",
							from: "MyMsg"
						},
						{
							msg:"hi",
							from: "OtherMsg"
						}
					],
					newMsgs:[
						{
							msg:"hello",
							from: "MyMsg"
						},
						{
							msg:"hi",
							from: "OtherMsg"
						}
					]

				},
				{
					name: "dummy1",
					msgs:[
						{
							msg:"hello1",
							from: "MyMsg",
							sender: "vimalnegi"
						},
						{
							msg:"hi1",
							from: "OtherMsg",
							sender: "aman"
						}
					]
				}
				],
				groups : [{
					name: "dummygroup",
					msgs:[
						{
							msg:"hello all",
							from: "out",
							sender: "user1"
						},
						{
							msg:"hi",
							from: "in",
							sender: "user2"
						}
					],
					newMsgs:[
						{
							msg:"hello",
							from: "MyMsg"
						},
						{
							msg:"hi",
							from: "OtherMsg"
						}
					]
				},
				{
					name: "dummy1group",
					msgs:[
						{
							msg:"hello1 all",
							from: "out",
							user: "user1"
						},
						{
							msg:"hi2",
							from: "in",
							user: "user1"
						}
					]
				}
				]
	}
}]);