var express = require('express');
var app=express();
var server = require('http').Server(app);

var io = require("socket.io")(server);
 
 app.use(express.static('public'));

 app.get('/', function(req, res) {
 	res.sendFile(__dirname+"/index.html");
 });

var chat = io.of('/chat');
var register = io.of('/register');
var users = [];
var rooms= {};
//var onlineUsers=[];
var connectedSockets= [];
var checkValidity = function(user, socket) {
		console.log('checkValidity called');
		if(users.indexOf(user) > -1)
		{
			console.log('valid user');
			connectedSockets[users.indexOf(user)] = socket.client.conn.id;
			return true;
		}
		return false;
	}

// function findClientsSocket(roomId, namespace) {
//     var res = [];
//     ns = io.of(namespace ||"/");    // the default namespace is "/"

//     if (ns) {
//         for (var id in ns.connected) {
//             if(roomId) {
//                 var index = ns.connected[id].rooms.indexOf(roomId) ;
//                 if(index !== -1) {
//                     res.push(ns.connected[id]);
//                 }
//             } else {
//                 res.push(ns.connected[id]);
//             }
//         }
//     }
//     return res;
// }


var handleChat =  function(socket) {

	console.log('handleChat called');

	socket.emit('giveUserName',"send user name for validation");

	socket.on('sendUserName', function(userName) {
		socket.userName = userName;
		console.log(userName);

		if(!checkValidity(userName, socket)) {
			chat.to("/chat#"+socket.client.conn.id).emit('error', 'something wrong happened');
			return;
		}

		
		chat.emit('userUpdated',users);
	 	console.log(socket.userName+' connected id is :- '+socket.id);
	 	// socket.emit('connected',{msg : "now you are connected"});

	 	socket.on('sendGroupMessage', function(data, success) {
	 		// console.log('got group msg');
	 		success(true);
	 		chat.to(data.id).emit('gotGroupMessage', { id : data.id, content : data.content, sender : socket.userName});
	 	});

	 	socket.on('disconnect', function(){
	 		console.log(socket.userName + " disconnected");
	 		var di = users.indexOf(socket.userName);
	 		users.splice(di,1);
	 		connectedSockets.splice(di,1);
	 		chat.emit('userUpdated',users);
	 	});

	 	socket.on('sendMessage', function(message, success) {
	 		var receiver = "/chat#"+connectedSockets[users.indexOf(message.userName)];
	 		chat.to(receiver).emit('newMessage', {userName : socket.userName, content : message.content});
	 		success(true);
	 	});


	 	socket.on('groupRequestAccepted', function(gid) {
	 		console.log('request Accepted');
	 		socket.join(gid);
	 		rooms[gid].push(socket.userName);
	 		chat.to(gid).emit('groupMembersUpdated',{id: gid, members : rooms[gid], sender : socket.userName});
	 	});


	 	socket.on('createGroup', function(group) {
	 		rooms[group.id] = [];
	 		rooms[group.id].push(socket.userName);
	 		socket.join(group.id);
	 		//io.sockets.sockets["/#"+connectedSockets[users.indexOf(group.user)]].join(group.id);
	 		var userSocket= "/chat#"+connectedSockets[users.indexOf(group.user)];
	 		chat.to(userSocket).emit('groupRequest',{id : group.id, members : rooms[group.id]});
	 		


	 		// res = findClientsSocket('group.id', '/chat');
	 		// console.log(res.length);
	 		// for(var i in res)
	 		// 	console.log(res[i]);

	 		//chat.to('/chat#'+).emit('acceptGroupRequest',);
	 		//console.log(chat.sockets.sockets["/chat#"+connectedSockets[users.indexOf(group.user)]]);
	 		// var a = io.sockets.sockets;
	 		// var keys = Object.keys(a);
	 		// console.log("no. of connected sockets:- "+keys.length);
	 		// for(var i=0; i<keys.length; i++)
	 		// 	console.log(keys[i]);
	 		//console.log(io.clients());
	 		
	 		console.log("request for creating "+group.id+" by "+socket.userName+" and add"+group.user );
	 	});

	 	socket.on('addUserToGroup', function(group) {
	 		// io.sockets.sockets["/#"+connectedSockets[users.indexOf(group.user)]].join(group.id);

	 		var userSocket= "/chat#"+connectedSockets[users.indexOf(group.user)];
	 		chat.to(userSocket).emit('groupRequest',{id:group.id, members : rooms[group.id]});

	 		
	 		// console.log(io.sockets.manager.rooms);


	 		// console.log(io.nsps['/chat'].adapter.rooms[group.id].length);
	 		// for(var i in io.nsps['/chat'].adapter.rooms[group.id])
	 		// 	console.log(i+" is "+io.nsps['/chat'].adapter.rooms[group.id][i]);
	 		// console.log(io.nsps['/chat'].adapter.rooms[group.id][0]);
	 		console.log(socket.userName + " wanted to add "+group.user+" to "+group.id);
	 	});
	});	
 };



var handleRegistration = function(socket)
{
	socket.on('register', function(userName, successful) {
		socket.userName = userName;
		users.push(socket.userName);
		connectedSockets.push(socket.client.conn.id);
		successful(true, socket.userName);
	});
};
register.on('connection',handleRegistration);
chat.on('connection', handleChat);

server.listen(8080);